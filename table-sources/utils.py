def get_id(val, ids):
    if val in ids:
        cid = ids.index(val)
    else:
        cid = len(ids)
        ids.append(val)

    return cid
