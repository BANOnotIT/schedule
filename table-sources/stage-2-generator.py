import argparse
import json
import sys

from utils import get_id

GROUPS_SEP = '=====  GROUPS  ====='
ROOMS_SEP = '=====  ROOMS  ====='

parser = argparse.ArgumentParser()
parser.add_argument('schedule', nargs='?', type=argparse.FileType('r'), default=sys.stdin)
parser.add_argument('ids_map', nargs='?', type=argparse.FileType('r'), default='2-ids-map.csv')
parser.add_argument('result', nargs='?', type=argparse.FileType('w'), default=sys.stdout)

args = parser.parse_args()

# values

schedule = json.load(args.schedule)

teacher_ids = []
group_ids = []
room_ids = []

# program

a= [teacher_ids,group_ids,room_ids]

delta=0
for line in args.ids_map:
    line = line.strip()

    if line == GROUPS_SEP:
        if delta!=0:
            raise Error('Section GROUPS already done')
        delta=1
    elif line == ROOMS_SEP:
        if delta !=1:
            raise Error('Section ROOMS failed to parse')
        delta=2
    else:
        a[delta].append(line)




days = len(schedule['days'])
teachers = {
    tid:[] for tid in range(len(teacher_ids))
}

maxes = [0]*days

for did, day in enumerate(schedule['days']):
    for lid, lesson in enumerate(day):
        for rid, pair in lesson.items():
            rid = int(rid)
            tid, gid = pair



            while len(teachers[tid]) <= did:
                teachers[tid].append({})

            teachers[tid][did][lid] = (gid,rid)

            maxes[did] = max(maxes[did], lid)



cols = ['Преподаватель']
for i, day  in enumerate(('Понедельник','Вторник','Среда','Четверг','Пятница')):
    cols.append(day)
    cols += ['']*maxes[i]

print(','.join(cols),file=args.result)

for_sort = sorted(list(teacher_ids))
sorted_tid = []
for teacher in for_sort:
    sorted_tid.append(teacher_ids.index(teacher))

for i, tid in enumerate(sorted_tid,1):
    cols = ['{}. {}'.format(str(i), teacher_ids[tid])]

    for did, count in enumerate(maxes):
        for lid in range(count):
            if did >= len(teachers[tid]) or lid not in teachers[tid][did]:
                cols.append('')
                continue


            pair = teachers[tid][did].get(lid)

            gid,rid = pair

            cols.append(group_ids[gid])
    print(','.join(cols),file=args.result)


