import argparse
import json
import sys

from utils import get_id

GROUPS_SEP = '=====  GROUPS  ====='
ROOMS_SEP = '=====  ROOMS  ====='

parser = argparse.ArgumentParser()
parser.add_argument('plan_file', nargs='?', type=argparse.FileType('r'), default=sys.stdin)
parser.add_argument('default_config', nargs='?', type=argparse.FileType('r'), default='0-billet-config.json')
parser.add_argument('rooms', nargs='?', type=argparse.FileType('r'), default='0-room-binding.csv')
parser.add_argument('mutator_conf', nargs='?', type=argparse.FileType('w'), default='1-operating-config.json')
parser.add_argument('ids_tmp', nargs='?', type=argparse.FileType('w'), default='2-ids-map.csv')

args = parser.parse_args()

# values

default_conf = json.load(args.default_config)
default_conf['pairs'] = []

teacher_ids = []
group_ids = []
room_ids = []

# program

batch = []

for line in args.plan_file:
    line = line.strip()

    teacher, group, hours, batch_size = line.split(',')

    tid = get_id(teacher, teacher_ids)
    gid = get_id(group, group_ids)

    hours = int(hours)

    default_conf.get('pairs').append({'teacher': tid,
                                      'group': gid,
                                      'count': hours})
    if batch_size.isdigit():
        batch_size = int(batch_size)
        batch.append(
            "%s-%s * %s" % (gid, tid, batch_size)
        )

if args.rooms:
    rooms_rule = []
    for line in args.rooms:
        line = line.strip()

        teacher, room = line.split(',')

        rid = get_id(room, room_ids)

        if teacher:
            tid = teacher_ids.index(teacher)
            rooms_rule.append('{} -> {}'.format(tid, rid))

    # Записываем количество доступных кабинетов
    default_conf['rooms'] = len(room_ids)

    # Записываем в правило для кабинетов
    if 'teacherRoomBinding' in default_conf.get('raters'):
        default_conf['raters']['teacherRoomBinding']['options'] = rooms_rule

if 'lessonsBatchSize' in default_conf.get('raters'):
    default_conf['raters']['lessonsBatchSize']['options'] = batch

# Сохраняем конфиг в файл
json.dump(default_conf, args.mutator_conf, indent=4)

ids_content = (
        '\n'.join(teacher_ids) +
        '\n' + GROUPS_SEP + '\n' +
        '\n'.join(group_ids) +
        '\n' + ROOMS_SEP + '\n' +
        '\n'.join(room_ids)
)

print(ids_content, end='', file=args.ids_tmp)
