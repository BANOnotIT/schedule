#[macro_use]
extern crate clap;
#[macro_use]
extern crate human_panic;
extern crate crossterm;
extern crate itertools;
extern crate once_cell;
extern crate rand;
extern crate rayon;
extern crate serde;
extern crate serde_json;

use std::io::{stdout, Read, Write};

use once_cell::sync::OnceCell;
use rayon::prelude::*;

use config_models::Config;
use crossterm::{style, ClearType, Color, Crossterm, Screen, TerminalInput};
use models::{RoomKey, Schedule};
use population::{Population, PopulationItem};
use ratings::Rating;

/// Модуль парсинга консольных аргументов
mod args;
/// Модуль парсинга конфигурации из файла
///
/// !!! Во всех настройках используйте [lowerCamelCase](https://ru.wikipedia.org/wiki/CamelCase) !!!
mod config_models;
/// Модели особи и примитивов
mod models;
/// Реализация мутации особи
mod mutator;
/// Модель популяции у тилитарные функции к ней
mod population;
/// Критериальные функции, используемые для композиции в конечную оптимизируемую функцию
mod ratings;
#[cfg(test)]
mod test;
/// Утилитарные функции ввода-вывода
mod utils;

static AVAILABLE_ROOM_KEYS: OnceCell<Vec<RoomKey>> = OnceCell::INIT;

fn main() {
    setup_panic!();

    let arguments = args::get_args_app();

    utils::print_header("Starting");

    let filepath = arguments.value_of("input").unwrap();

    print!("  Reading config file\n\r");

    let config = match utils::read_config_from_file(filepath) {
        Err(err) => {
            eprintln!("{}", err);
            return;
        }
        Ok(conf) => conf,
    };

    {
        let available_rooms = config.gen_room_keys();
        if available_rooms.is_empty() {
            eprintln!("  Rooms range should be specified");
            return;
        }

        Schedule::set_available_room_keys(available_rooms)
    }

    println!("  Room keys generated");

    // check config
    if config.raters.is_empty() {
        eprintln!("\"rules\" section must be specified");
        return;
    }

    // Prepare Step
    utils::print_header("Preparing simulation");

    print!("  Reading rater rules\n\r");
    let rater = ratings::Rating::new(&config);

    let mut population = if let Some(path) = arguments.value_of("population") {
        println!("  Loading species...");
        let species = match utils::load_species(path) {
            Err(err) => {
                eprintln!("{}", err);
                return;
            }
            Ok(conf) => conf,
        };
        rate_species(&rater, species)
    } else {
        println!("  Generating species...");
        rate_species(
            &rater,
            generate_initial_species(config.advanced.base_population_size, &config),
        )
    };

    println!("  Population size: {}", &population.len());

    // Life cycle
    let mut stats_file: Option<utils::StatLogger> = match arguments.value_of("stat") {
        Some(path) => {
            print!("  Using statistics file: {}\n\r", path);
            print!("    format: max/median/min\n\r");
            let logger = utils::StatLogger::new(path);
            match logger {
                Err(err) => {
                    eprintln!("{:}", err.description());
                    return;
                }
                Ok(conf) => Some(conf),
            }
        }
        None => None,
    };

    let dump_population_frequently =
        config.advanced.dump_freq != 0 && !config.advanced.dump_file.is_empty();
    if dump_population_frequently {
        println!(
            "  Dumping population every {} cycles in `{}`",
            config.advanced.dump_freq, &config.advanced.dump_file
        );
    }

    utils::print_header("Simulation");
    utils::print_live_commands();

    let screen = Screen::new(true);
    let crossterminal = Crossterm::from_screen(&screen);
    let input = TerminalInput::from_output(&screen.stdout);
    let term = crossterminal.terminal();
    let mut stdin = input.read_async().bytes();

    let population_len = config.advanced.base_population_size;
    let frontier_steps_count = population_len as u64 / 4;

    let mut idle_steps: u64 = 0;
    let mut steps_in_general: u64 = 0;

    let mutations_per_child = config.advanced.mutations_per_child as f32;

    // Cross thread vars

    println!("  Simulation started...");
    // Lifecycle loop
    loop {
        if idle_steps == frontier_steps_count {
            let min_convergence = min_convergence_percent(&population);
            if min_convergence > config.advanced.breaking_convergence {
                break;
            }

            idle_steps = 0;
        }

        let min_rate = population[population_len - 1].0.clone();

        // Joining candidates from thread
        let candidates: Vec<PopulationItem> = population
            .par_iter()
            .enumerate()
            .flat_map(|(i, first)| {
                population.par_iter().enumerate().map(move |(j, second)| {
                    let presumed_position = (i + j) / 2;

                    let shift = (presumed_position as f32) / (population_len as f32);

                    let mutations = mutations_per_child * shift.powi(2);

                    let mut child = Schedule::from_parents(&first.1, &second.1);
                    for _ in 0..=mutations.trunc() as u64 {
                        child.mutate();
                    }

                    child
                })
            })
            .filter_map(|item| {
                let rate = rater.calc(&item);
                if rate > min_rate {
                    Some(PopulationItem(rate, Box::new(item)))
                } else {
                    None
                }
            })
            .collect();

        if candidates.is_empty() {
            idle_steps += 1;
        } else {
            idle_steps = 0;
        }

        // Inserting children in population
        for child in candidates {
            population::insert_and_shift(&mut population, child);
        }

        steps_in_general += 1;

        stdout().flush().unwrap();
        print!(
            "\r  Simulating {}",
            utils::gen_loader(
                steps_in_general,
                (idle_steps * 8 / frontier_steps_count) as u8,
            )
        );

        // Writing stats to log file
        match &mut stats_file {
            Some(log) => log.write(&population),
            None => (),
        }

        if dump_population_frequently && steps_in_general % config.advanced.dump_freq == 0 {
            println!(
                "\r  Dumping population into `{}`",
                &config.advanced.dump_file
            );
            if utils::dump_population(&config.advanced.dump_file, &population).is_err() {
                println!("\r  Failed to dump population")
            }
            let len = population.len();
            print!(
                "\r    Current Stats: {}/{}/{}\n\r",
                population[0],             // max
                population[(len - 1) / 2], // median
                population[len - 1]        // min
            );
            let convergence = min_convergence_percent(&population);
            print!("    Convergence: {}%\n\r", convergence);
        }

        if let Some(Ok(cmd)) = stdin.next() {
            let _ = term.clear(ClearType::CurrentLine);
            match cmd {
                b'q' | b'Q' | b'c' | b'C' => {
                    print!(
                        "\r  {}\n\r",
                        style("Quiting simulation loop").bold().with(Color::Red)
                    );
                    break;
                }

                b'd' | b'D' => {
                    if utils::dump_population(&config.advanced.dump_file, &population).is_ok() {
                        print!(
                            "\r    Current population dumped into `{}`\n\r",
                            &config.advanced.dump_file
                        );
                    } else {
                        print!("\r    Failed to dump population\n\r");
                    }
                }

                b's' | b'S' => {
                    let len = population.len();
                    print!(
                        "\r    Current Stats: {}/{}/{}\n\r",
                        population[0],             // max
                        population[(len - 1) / 2], // median
                        population[len - 1]        // min
                    );
                    let convergence = min_convergence_percent(&population);
                    print!("    Convergence: {}%\n\r", convergence);
                }

                _ => (),
            }
        }
    }

    let _ = term.clear(ClearType::CurrentLine);
    print!("  Maximum found: {:?}\n\r", &population[0].0);
    print!("  Min: {:?}\n\r", &population[population_len - 1].0);

    // writing to file

    utils::print_header("Writing to file");
    let most = &population[0];
    let a = utils::write_result(arguments.value_of("output").unwrap(), &most.1);
    if let Err(e) = a {
        eprintln!("{}", e);
        return;
    };
}

/// Генерация N количества случайных особей
fn generate_initial_species(count: usize, config: &Config) -> Vec<Schedule> {
    use models::{RoomKey, Setter};

    let mut population = Vec::with_capacity(count as usize);

    let pairs = config.get_pairs();

    let lessons_per_day = config.lessons_per_day * config.rooms;
    let maximum_lessons_per_spc = lessons_per_day * config.days;

    for _ in 0..count {
        let mut spcm = Schedule {
            days: Vec::with_capacity(config.days),
        };

        for _ in 0..config.days {
            spcm.days.push(Vec::new());
        }

        for (_, pair) in (&pairs).iter().enumerate() {
            for i in 0..maximum_lessons_per_spc {
                let day = i / lessons_per_day;
                let lesson = i % lessons_per_day / config.rooms;
                let room = i % lessons_per_day % config.rooms;
                let key = RoomKey(day, lesson, room);

                if spcm.can_insert(*pair, key) {
                    let _ = spcm.insert_item(key, *pair);
                    break;
                }
            }
        }

        // 2k изначальных мутаций...
        for _ in 0..2000 {
            spcm.mutate();
        }

        population.push(spcm);
    }

    population
}

/// Инициализация первой популяции
/// Создаёт N случайных особей и сортирует их по рейтингу
fn rate_species(rater: &Rating, species: Vec<Schedule>) -> Population {
    // Сразу указываем 1 место для лишнего элемента.
    // Чтобы было куда вставлять потом без лишних аллокаций
    let mut base_population = Population::with_capacity(species.len() + 1);

    for pair in species {
        let rating = rater.calc(&pair);
        base_population.push(PopulationItem(rating, Box::new(pair)));
    }

    // Вместо прямой сортировки мы будем использовать обратную, чтобы самая лучшая особь
    //  была ближе к началу
    base_population.sort_by(|a, b| a.cmp(&b).reverse());

    base_population
}

/// Подсчёт минимального схождения в популяции. Считается в целых процентах
fn min_convergence_percent(pop: &Population) -> usize {
    use models::Diff;

    let count = pop[0].1.get_pairs().len();
    let len = pop.len();

    let mut min_percent = 100;

    for i in 0..len {
        for j in i..len {
            let a = &pop[i];
            let b = &pop[j];

            let differences = a.1.diff(&*b.1);
            let percents = differences.0.len() * 100 / count;

            if percents < min_percent {
                min_percent = percents;
            }
        }
    }

    min_percent
}
