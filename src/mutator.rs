use models::{Coition, Schedule, Setter};

impl Schedule {
    /// Получение новой особи путём скрещивания родителей
    pub fn from_parents(first: &Self, second: &Self) -> Self {
        let (mut child, lessons) = first.get_common_diff(second);

        for lesson in lessons {
            loop {
                let key = Self::get_random_room_key();

                if child.can_insert(lesson, key) {
                    let _ = child.insert_item(key, lesson);
                    break;
                }
            }
        }

        child
    }

    /// Одна мутация в фенотипе особи
    pub fn mutate(&mut self) {
        let donor = loop {
            let a = Self::get_random_room_key();

            let elem = self.remove_keys(&vec![a])[0];
            if elem.is_some() {
                break elem.unwrap();
            }
        };

        let new_room = loop {
            let a = Self::get_random_room_key();
            if self.can_insert(donor, a) {
                break a;
            }
        };

        let _ = self.insert_item(new_room, donor);
    }
}
