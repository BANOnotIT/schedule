use config_models::Config;
use models::Schedule;

use self::traits::RatingFn;

/// # Количество уроков в день у учеников (`dayLen`)
/// Считает количества уроков, у учеников. Направлен на приближение всех уроков к началу дня.
mod day_len;

/// Нулевой оценщик. Fallback к любому неопознаному оценщику.
mod default_rater;

/// # Разнообразность уроков у учеников (`lessonsBatchSize`)
/// Считает количество одинаковых уроков идущих подряд. Если оно больше задаваемого порога,
/// то вычитает из общего зачёта отклонение от этого порога. Стандартный порог - 1 урок.
///
/// Формат опций: `<group id>-<teacher id> * <batch size>`
mod lessons_batch_size;

/// # Уменьшение количества окон у учеников (`lessonsSeq`)
/// Считает количество "окон в расписании"
mod lessons_seq;

/// # Методические дни (`teacherFreeDay`)
/// Считает количество методических дней для учителей
mod teacher_free_days;

/// # Стационарность учителей (`teacherMoves`)
/// Считает количества передвижений, которые совершают учителя между уроками
mod teacher_immovability;

/// # Привязка кабинетов к учителям (`teacherRoomBinding`)
/// Считает количества уроков, которые учитель с кабинетом провёл вне своего кабинета.
/// Формат опций: "<id учителя> -> <id кабинета>", пробелы рядом вокруг стрелочки обязательны.
mod teacher_room_binding;

mod traits;

pub struct Rating {
    rank_functions_with_mass: Vec<Vec<(f32, Box<dyn RatingFn>)>>,
    total_mass_by_rank: Vec<f32>,
}

impl Rating {
    pub fn new(conf: &Config) -> Rating {
        let mut rank_functions_with_mass: Vec<Vec<(f32, Box<dyn RatingFn>)>> =
            (0..conf.get_ranks_count()).map(|_| Vec::new()).collect();

        let mut total_mass_by_rank = vec![0.0; conf.get_ranks_count()];

        for (key, rule) in conf.raters.iter() {
            let default_options = Vec::new();
            let options = match &rule.options {
                Some(a) => a,
                None => &default_options,
            };

            let function: Box<dyn RatingFn> = match key.as_str() {
                "dayLen" => day_len::DayLen::new(conf, options),
                "lessonsSeq" => lessons_seq::LessonsSeq::new(conf, options),
                "teacherRoomBinding" => {
                    teacher_room_binding::TeacherRoomBinding::new(conf, options)
                }
                "teacherImmovability" => {
                    teacher_immovability::TeacherImmovability::new(conf, options)
                }
                "teacherFreeDays" => teacher_free_days::TeacherFreeDay::new(conf, options),
                "lessonsBatchSize" => lessons_batch_size::LessonsBatchSize::new(conf, options),
                unattended_rater_name => {
                    println!(
                        "    No rating rule \"{}\" found in system, using 0 instead",
                        unattended_rater_name
                    );
                    default_rater::Default::new(conf, options)
                }
            };

            rank_functions_with_mass[rule.rank].push((rule.mass as f32, function));

            let rank_mass =
                total_mass_by_rank.get(rule.rank).cloned().unwrap_or(0.0) + rule.mass as f32;
            total_mass_by_rank[rule.rank] = rank_mass;
        }

        Self {
            rank_functions_with_mass,
            total_mass_by_rank,
        }
    }

    pub fn calc(&self, schd: &Schedule) -> Vec<f32> {
        let mut result = Vec::new();

        for (raters_in_rank, &rank_total_mass) in self
            .rank_functions_with_mass
            .iter()
            .zip(self.total_mass_by_rank.iter())
        {
            let mut rate = 0.0;

            for (mass, rater) in raters_in_rank {
                rate += *mass * rater.rate(schd) / rank_total_mass;
            }
            result.push(rate);
        }

        result
    }
}

mod utils;
