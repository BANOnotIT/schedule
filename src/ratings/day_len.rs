use std::collections::{BTreeMap, HashMap};

use crate::models::{GroupId, LessonPair, Schedule};

use self::super::traits::RatingFn;
use config_models::{Config, Pair};

/// # Количество уроков в день у учеников (`dayLen`)
/// Считает количества уроков у учеников. Направлен на приближение всех уроков к началу дня.
#[derive(Debug)]
pub struct DayLen {
    pub distinction: f32,
    pub best: f32,
    pub worst: f32,
}

impl RatingFn for DayLen {
    fn new(conf: &Config, _: &Vec<String>) -> Box<Self> {
        let mut groups = HashMap::new();
        for &Pair { count, group, .. } in &conf.pairs {
            if groups.contains_key(&group) {
                let count = count + groups[&group];
                groups.insert(group, count);
            } else {
                groups.insert(group, count);
            }
        }

        let all_lessons_count = groups.values().cloned().sum::<usize>();

        let best = all_lessons_count as f32;

        let worst = {
            let mut lopped_groups: Vec<_> = groups
                .values()
                .map(|&lessons_count| {
                    if lessons_count >= conf.days {
                        conf.days
                    } else {
                        lessons_count
                    }
                })
                .collect();

            let mut result = 0;
            for _ in 0..conf.days {
                let mut i = 0;
                for value in lopped_groups.iter_mut() {
                    if *value > 0 {
                        *value -= 1;
                        i += 1;
                        result += conf.lessons_per_day - (i / conf.rooms)
                    }
                }
            }

            result as f32
        };

        Box::new(Self {
            distinction: worst - best,
            best,
            worst,
        })
    }

    fn rate(&self, schd: &Schedule) -> f32 {
        let mut result = 0.0;
        let mut map: BTreeMap<GroupId, f32> = BTreeMap::new();
        for day in &schd.days {
            for (i, lesson) in day.iter().enumerate() {
                for &LessonPair(_, gid) in lesson.values() {
                    map.insert(gid, i as f32);
                }
            }

            result += map.values().sum::<f32>() as f32;
            map.clear();
        }

        (self.worst - result) / self.distinction
    }
}

#[test]
fn initialization() {
    let config: Config = serde_json::from_str(
        r#"
            {
                "pairs": [{"teacher":0,"group":0,"count":10},{"teacher":0,"group":1,"count":10},{"teacher":0,"group":3,"count":1}],
                "raters": { },
                "days": 2,
                "rooms": 10,
                "lessonsPerDay": 10
            }"#,
    )
        .unwrap();

    let rater = DayLen::new(&config, &vec![]);

    let worst = 50.0;
    let best = 21.0;

    assert_eq!(rater.distinction, worst - best);
    assert_eq!(rater.best, best);
    assert_eq!(rater.worst, worst);
}
