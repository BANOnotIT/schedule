use std::collections::BTreeMap;

use models::{GroupId, LessonPair, RoomKey, Schedule, TeacherId};

use self::super::traits::RatingFn;
use config_models::Config;
use std::collections::HashMap;

type OptimizedTree = BTreeMap<(TeacherId, GroupId), u8>;

/// # Разнообразность уроков у учеников (`lessonsBatchSize`)
/// Считает количество одинаковых уроков идущих подряд. Если оно больше задаваемого порога,
/// то вычитает из общего зачёта отклонение от этого порога. Стандартный порог - 1 урок.
///
/// Формат опций: `<group id>-<teacher id> * <batch size>`. Пробелы обязательны
pub struct LessonsBatchSize(pub OptimizedTree, pub u32);

fn optimize_options_to_pair_to_count_map(options: &Vec<String>) -> OptimizedTree {
    let mut map = BTreeMap::new();

    for (i, option) in options.iter().enumerate() {
        let nums: Vec<&str> = option.split(" * ").collect();

        if nums.len() != 2 {
            eprintln!(
                "Skipping {}-th option of \"lessons-batch-size\" 'case of wrong format",
                i
            );
            continue;
        }

        let count = match nums[1].parse::<u8>() {
            Ok(tid) => tid,
            Err(e) => {
                eprintln!(
                    "Error while parsing BatchLength option {} of \"lessons-batch-size\": {:}",
                    i, e
                );
                eprintln!("Skipping option {}", i);
                continue;
            }
        };

        let ids: Vec<&str> = nums[0].split('-').collect();
        if ids.len() != 2 {
            eprintln!(
                "Skipping {}-th option of \"lessons-batch-size\" 'case of wrong format",
                i
            );
            continue;
        }

        let tid: TeacherId = match ids[0].parse::<usize>() {
            Ok(tid) => tid,
            Err(e) => {
                eprintln!(
                    "Error while parsing TID option {} of \"lessons-batch-size\" {:}",
                    i, e
                );
                eprintln!("Skipping option {}", i);
                continue;
            }
        };
        let gid: GroupId = match ids[1].parse::<usize>() {
            Ok(tid) => tid,
            Err(e) => {
                eprintln!(
                    "Error while parsing GID option {} of \"lessons-batch-size\" {:}",
                    i, e
                );
                eprintln!("Skipping option {}", i);
                continue;
            }
        };

        map.insert((tid, gid), count);
    }
    map
}

impl RatingFn for LessonsBatchSize {
    fn new(conf: &Config, options: &Vec<String>) -> Box<Self> {
        let map = optimize_options_to_pair_to_count_map(options);

        let worst: u32 = conf
            .pairs
            .iter()
            .map(|pair| {
                let optimized_lessons_count = match map.get(&(pair.teacher, pair.group)) {
                    Some(&a) => (pair.count / a as usize) + (pair.count % a as usize),
                    None => pair.count,
                };

                optimized_lessons_count as u32 - 1
            })
            .sum();

        Box::new(Self(map, worst))
    }

    fn rate(&self, schd: &Schedule) -> f32 {
        let mut result = 0;

        let mut hash: HashMap<(TeacherId, GroupId, usize), u8> = HashMap::new();
        let mut last_day: usize = 0;
        for (RoomKey(day, lesson, _), LessonPair(tid, gid)) in &schd.get_pairs() {
            if *day != last_day {
                hash = HashMap::new();
                last_day = *day;
            }

            let key = (*tid, *gid);

            // Если урок первый, то выдаём сразу нуль, чтобы не было переполнения по нижней границе
            let already = match *lesson {
                0 => 0,
                _ => *hash.get(&(*tid, *gid, *lesson - 1)).unwrap_or(&0),
            };
            let max = *self.0.get(&key).unwrap_or(&1);

            if already + 1 > max {
                result += u32::from(already + 1 - max);
            }

            hash.insert((*tid, *gid, *lesson), already + 1);
        }

        (self.1 - result) as f32 / self.1 as f32
    }
}

#[test]
fn initialization_without_options() {
    use serde_json;

    let conf: Config = serde_json::from_str(
        r#"
{
    "pairs": [{"teacher":0,"group":0,"count":10},{"teacher":0,"group":1,"count":10}],
    "raters": { },
    "days": 2,
    "rooms": 10,
    "lessonsPerDay": 10
}
"#,
    )
    .unwrap();

    let rater = LessonsBatchSize::new(&conf, &vec![]);

    assert_eq!(rater.1, 9 + 9);
}

#[test]
fn initialization_with_options() {
    use serde_json;

    let conf: Config = serde_json::from_str(
        r#"
{
    "pairs": [{"teacher":0,"group":0,"count":10},{"teacher":0,"group":1,"count":10}],
    "raters": { },
    "days": 2,
    "rooms": 10,
    "lessonsPerDay": 10
}
"#,
    )
    .unwrap();

    let rater = LessonsBatchSize::new(&conf, &vec!["0-0 * 2".to_owned(), "0-1 * 3".to_owned()]);

    assert_eq!(rater.1, 4 + 3);
}
