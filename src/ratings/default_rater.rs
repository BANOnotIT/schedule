use crate::models::Schedule;

use self::super::traits::RatingFn;
use config_models::Config;

/// Нулевой оценщик. Fallback к любому неопознаному оценщику.
pub struct Default;

impl RatingFn for Default {
    fn new(_: &Config, _: &Vec<String>) -> Box<Self> {
        Box::new(Default)
    }

    fn rate(&self, _: &Schedule) -> f32 {
        0.0
    }
}
