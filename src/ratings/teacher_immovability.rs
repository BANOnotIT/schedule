use std::collections::HashMap;

use models::{ClassroomId, LessonPair, RoomKey, Schedule, TeacherId};

use self::super::traits::RatingFn;
use config_models::{Config, Pair};

/// # Стационарность учителей (`teacherImmovability`)
/// Считает количества передвижений, которые совершают учителя между уроками
pub struct TeacherImmovability(pub f32);

impl RatingFn for TeacherImmovability {
    fn new(conf: &Config, _: &Vec<String>) -> Box<Self> {
        let best = conf
            .pairs
            .iter()
            .fold(0.0, |sum, &Pair { count, .. }| sum + count as f32);

        Box::new(Self(best as f32))
    }

    fn rate(&self, schd: &Schedule) -> f32 {
        let mut result = 0.0;
        let mut last_room: HashMap<TeacherId, ClassroomId> = HashMap::new();

        for (RoomKey(_, _, rid), LessonPair(tid, _)) in &schd.get_pairs() {
            if last_room.contains_key(tid) {
                if last_room.get(&tid) != Some(rid) {
                    result += 1.0;
                    last_room.insert(*tid, *rid);
                }
            } else {
                last_room.insert(*tid, *rid);
            }
        }

        result / self.0
    }
}

#[test]
fn rater() {
    use serde_json;

    // учитель 1 вынужден 1 перейти из кабинета 1 в кабинет 3
    let a: Schedule = serde_json::from_str(
        r#"{"days": [ [ { "1": [1, 1] }, { "2": [2, 1], "3": [1, 3] },{},{"3": [1, 2] } ] ]}"#,
    )
    .unwrap();

    let rater = TeacherImmovability(20.0);

    assert_eq!(rater.rate(&a), 1.0 / 20.0);
}

#[test]
fn initialization() {
    use serde_json;

    let conf: Config = serde_json::from_str(
        r#"
{
    "pairs": [{"teacher":1,"group":0,"count":10},{"teacher":2,"group":1,"count":10}],
    "raters": { },
    "days": 2,
    "rooms": 10,
    "lessonsPerDay": 10
}
"#,
    )
    .unwrap();

    let rater = TeacherImmovability::new(&conf, &vec![]);

    assert_eq!(rater.0, 20.0);
}
