use std::error::Error;
use std::fs::File;
use std::io::Write;

use config_models::Config;
use crossterm::{style, Color};
use models::Schedule;
use population::{Population, PopulationItem};
use std::fmt::Display;

/// Сгенерировать строчку загрузки, показывающую текущий процесс
pub fn gen_loader(steps: u64, same_steps: u8) -> String {
    let twig: Vec<char> = "-\\|/".chars().collect();

    let mut stars = String::new();
    for _ in 0..same_steps {
        stars.push('=');
    }

    format!(
        "{} [{:<8}] : {}",
        twig[steps as usize % twig.len()],
        style(stars).with(Color::Blue),
        steps
    )
}

/// Прочитать файл конфигурации из файла
pub fn read_config_from_file<P: AsRef<std::path::Path>>(path: P) -> Result<Config, Box<Error>> {
    use std::fs::File;
    use std::io::BufReader;
    // Open the file in read-only mode with buffer.
    let file = File::open(path)?;
    let reader = BufReader::new(file);

    // Read the JSON contents of the file as an instance of `Config`.
    let u = serde_json::from_reader(reader)?;

    // Return the `Config`.
    Ok(u)
}

/// Записать особь в файл
pub fn write_result<P: AsRef<std::path::Path>>(path: P, schd: &Schedule) -> Result<(), Box<Error>> {
    use std::fs::File;
    use std::io::BufWriter;

    let file = File::create(path)?;
    let writer = BufWriter::new(file);

    serde_json::to_writer_pretty(writer, schd)?;

    Ok(())
}

/// Записать популяцию в файл
pub fn dump_population<P: AsRef<std::path::Path>>(
    path: P,
    pop: &Population,
) -> Result<(), Box<Error>> {
    use std::fs::File;
    use std::io::BufWriter;

    let file = File::create(path)?;
    let writer = BufWriter::new(file);

    let mut items = Vec::new();

    for PopulationItem(_, schd) in pop {
        items.push(schd);
    }

    serde_json::to_writer(writer, &items)?;

    Ok(())
}

/// Прочесть популяцию из файла
pub fn load_species<P: AsRef<std::path::Path>>(path: P) -> Result<Vec<Schedule>, Box<Error>> {
    use std::fs::File;
    use std::io::BufReader;
    // Open the file in read-only mode with buffer.
    let file = File::open(path)?;
    let reader = BufReader::new(file);

    // Read the JSON contents of the file as an instance of `Config`.
    let u = serde_json::from_reader(reader)?;

    // Return the `Config`.
    Ok(u)
}

/// Показать все возможные live команды
///
/// Команды:
///  q или c - выйти из цикла симуляции
///  d - сохранить текущую популяцию в файл `population-dump.json`
///  s - показать текущие параметры популяции в log формате
pub fn print_live_commands() {
    use crossterm::{style, Color};
    let a = "  Available runtime commands:\n\r      Q or C  - quit simulation loop\n\r      D       - dump current population\n\r      S       - show current population statistics in max/mid/min format\n\r";

    print!("{}", style(a).with(Color::Yellow));
}

/// Функция печати заголовка этапе
pub fn print_header<D: Display>(a: D) {
    println!("{}", style(a).bold().with(Color::Cyan));
}

/// Логгер
/// При создании логгера указывается путь к файлу, в который нужно писать лог.
pub struct StatLogger {
    file: File,
}

impl StatLogger {
    pub fn new<P: AsRef<std::path::Path>>(path: P) -> Result<Self, Box<Error>> {
        let file = File::create(path)?;

        Ok(Self { file })
    }

    /// Метод записи лога в файл.
    ///
    /// Каждая строчка лога - один цикл, формат лога:
    /// `[max_rating]/[mid_rating]/[min_rating]`. Каждый рейтинг - массив чисел через запятую
    pub fn write(&mut self, population: &Population) {
        let len = population.len();

        let _ = writeln!(
            self.file,
            "{}/{}/{}",
            population[0],             // max
            population[(len - 1) / 2], // median
            population[len - 1]        // min
        );
    }
}
