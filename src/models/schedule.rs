use self::super::prims::*;
use self::super::traits::{Coition, Diff, Setter};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use AVAILABLE_ROOM_KEYS;

/// Особь расписания
#[derive(Clone, Deserialize, Serialize, Eq, PartialEq, Debug)]
pub struct Schedule {
    /// Массив дней, длинна которого равна config.days
    pub days: Vec<Day>,
}

/// Пара ключ-значения, для удобства итерации
type Pair = (RoomKey, LessonPair);

impl Schedule {
    pub fn set_available_room_keys(keys: Vec<RoomKey>) {
        AVAILABLE_ROOM_KEYS.set(keys).unwrap();
    }
    pub fn get_available_room_keys() -> &'static Vec<RoomKey> {
        &AVAILABLE_ROOM_KEYS
            .get()
            .expect("AVAILABLE_ROOM_KEYS isn't initialized")
    }
    pub fn get_random_room_key() -> RoomKey {
        use rand::seq::IteratorRandom;
        use rand::thread_rng;
        let mut rng = thread_rng();

        *Self::get_available_room_keys()
            .iter()
            .choose(&mut rng)
            .unwrap()
    }

    /// Функция получения урока по его ключу
    pub fn get_item(&self, key: RoomKey) -> Option<&LessonPair> {
        let RoomKey(day, lesson, room) = key;
        self.days
            .get(day)
            .and_then(|day| day.get(lesson))
            .and_then(|lesson| lesson.get(&room))
    }

    /// Можно ли вставить определённую пару в определённую ячейку
    ///
    /// false, если указан день, не входящих в область определения
    /// true, если в указанном дне ещё нет такого номера уроков
    /// false, если комната занята или учитель или группа в это время не свободны от уроков
    pub fn can_insert(&self, pair: LessonPair, key: RoomKey) -> bool {
        // Проверяем, что в конкретное время и конкретный кабинет мы можем вставить нашу ячейку

        let RoomKey(day, lesson, room) = key;
        let LessonPair(teacher, group) = pair;

        // Если дня в принципе не существует в нашем росписании
        let day: Option<&Day> = self.days.get(day);
        if day.is_none() {
            return false;
        }

        // Если такого урока ещё не существует в сетке
        let lesson: Option<&Lessons> = day.unwrap().get(lesson);
        if lesson.is_none() {
            return true;
        }

        let lesson = lesson.unwrap();

        // Если комната уже занята
        if lesson.contains_key(&room) {
            return false;
        }

        // Если какой=то из участников процесса уже находится науроке
        let contains_any_member = lesson
            .values()
            .any(|&LessonPair(t_id, g_id)| (t_id == teacher) || (g_id == group));

        !contains_any_member
    }

    /// Представить расписание в виде массива пар "ключ-значение"
    pub fn get_pairs(&self) -> Vec<Pair> {
        let mut result = Vec::new();

        for (day_id, day) in self.days.iter().enumerate() {
            for (lesson_id, lesson) in day.iter().enumerate() {
                for (&room_id, &pair) in lesson.iter() {
                    result.push((RoomKey(day_id, lesson_id, room_id), pair))
                }
            }
        }

        result
    }
}

impl Setter for Schedule {
    type Item = LessonPair;
    type Key = RoomKey;
    fn insert_item(&mut self, key: Self::Key, item: Self::Item) -> Result<(), ()> {
        let RoomKey(day, lesson, room) = key;

        // Если дня в принципе не существует в нашем росписании
        let day: Option<&mut Day> = self.days.get_mut(day);
        if day.is_none() {
            return Err(());
        }
        let day = day.unwrap();

        if day.len() < lesson + 1 {
            for _ in 0..=lesson - day.len() {
                day.push(HashMap::new())
            }
        }

        let lesson: &mut Lessons = day.get_mut(lesson).unwrap();

        if lesson.contains_key(&room) {
            return Err(());
        }

        lesson.insert(room, item);

        Ok(())
    }
}

impl Coition for Schedule {
    fn remove_keys(&mut self, diff: &Vec<RoomKey>) -> Vec<Option<LessonPair>> {
        let mut result = Vec::new();

        for &RoomKey(day, lesson, room) in diff {
            result.push(
                self.days
                    .get_mut(day)
                    .and_then(|day| day.get_mut(lesson))
                    .and_then(|a| a.remove(&room)),
            )
        }

        result
    }
}

impl Diff for Schedule {
    type Item = LessonPair;
    type Key = RoomKey;
    fn diff(&self, other: &Self) -> (Vec<RoomKey>, Vec<LessonPair>) {
        let mut ids = vec![];
        let mut lessons = vec![];

        let a = self.days.iter().zip(other.days.iter()).enumerate();
        for (day_id, (day_a, day_b)) in a {
            let (rooms, mut lessons_diff) = day_a.diff(&day_b);

            for (lesson_id, room_id) in rooms {
                ids.push(RoomKey(day_id, lesson_id, room_id))
            }
            lessons.append(&mut lessons_diff);
        }

        (ids, lessons)
    }
}
