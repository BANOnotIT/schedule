# Генератор расписаний
Программа для составления расписаний при помощи подбора лучшего, подходящего условиям.

# Скачать
- [Linux archive x64 (.tar.gz)](./schedule.linux.tar.gz)
- [Windows x64 (.exe)](./schedule.exe)